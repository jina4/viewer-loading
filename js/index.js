var viewer = null;
let loadMask = document.getElementById("loadmask");
let persentege=document.getElementById("persentage")

function initBim() {
  loadMask.classList.remove("hidden");

  var config = {
    extensions: ["Autodesk.Viewing.ZoomWindow"],
    disabledExtensions: {
      measure: false,
      section: false,
    },
    memory: {
      limit: 32 * 1024, //32 GB
    },
  };

  var viewerDiv = document.getElementById("viewerDiv");
  viewer = new Autodesk.Viewing.Private.GuiViewer3D(viewerDiv, config);
  //var viewer = new Autodesk.Viewing.Viewer3D(viewerDiv, config);

  persentege.innerText="0%"

  var options = {
    env: "Local",
    offline: "true",
    useADP: false,
    //applyScaling: 'm', //unit 強制換為m
    docid: './static/model/3d.svf'
  };

  Autodesk.Viewing.Initializer(options, function () {
    var startedCode = viewer.start();
    if (startedCode > 0) {
      console.error("Failed to create a Viewer: WebGL not supported.");
      return;
    }
    viewer.loadModel(
      options.docid,
      {
        isAEC: true,
      },
      onLoadModelSuccess,
      onLoadModelError
    );
  });

  viewer.addEventListener(
    Autodesk.Viewing.GEOMETRY_LOADED_EVENT,
    onGeometryLoadedEvent
  );

  viewer.addEventListener("propertyDbLoaded", ()=>{
    persentege.innerText="60%"
  });
}

function onLoadModelSuccess(event) {
  console.log("success");persentege.innerText="10%"
}

function onLoadModelError(event) {
  console.log("fail");
}

function onGeometryLoadedEvent(event) {
  console.log("onGeometryLoadedEvent");

  loadMask.animate(
    [{ opacity: 1, offset: 0.4 }, { opacity: 0 }],
    {
      duration: 4000,
      fill: "forwards",
      easing: "steps(2,end)",
      iterations: 1,
    }
  );

  persentege.innerText="85%"

  setTimeout(() => {
    persentege.innerText="100%"
    loadMask.classList.add("hidden");
  }, 4000);
  
}

initBim();
